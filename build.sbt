name := "sandbox"

version := "1.0"

scalaVersion := "2.11.7"

scalacOptions += "-Xfatal-warnings"

resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"

libraryDependencies += "org.scalactic" %% "scalactic" % "2.2.6"
libraryDependencies += "org.scalatest" %% "scalatest" % "2.2.6" % "test"
libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "1.0.2"