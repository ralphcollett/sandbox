package sandbox

class Dog

object GreetableImplicits {

  trait Greetable {
    def greet(): String
  }

  implicit class DogMaker(dog: Dog) extends Greetable {
    override def greet(): String = "woof!"
  }
}

object TypeClass extends App {

  import GreetableImplicits._

  println(new Dog().greet())
}

