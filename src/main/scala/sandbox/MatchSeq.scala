package sandbox

object MatchSeq extends App  {

  def buggy[A](seq: Seq[A]) = {
    seq match {
      case Nil => "empty"
      case _ :: Nil => "one entry"
      case _ => "multiple entries"
    }
  }

  def fixed[A](seq: Seq[A]) = {
    seq match {
      case Seq() => "empty"
      case Seq(_) => "one entry"
      case _ => "multiple entries"
    }
  }

  println(buggy(List("a"))) // one entry
  println(buggy(Seq("a"))) // one entry
  println(buggy(Stream.cons("a", Stream.empty))) // multiple entries

  println(fixed(List("a"))) // one entry
  println(fixed(Seq("a"))) // one entry
  println(fixed(Stream.cons("a", Stream.empty))) // one entry
}
