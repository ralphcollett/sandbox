package sandbox

object LambdasAndClosures {

  val a: Int = ???

  // can access variables defined outside lexical scope
  val closure = (b: Int) => a + b

  // can only access parameters
  val lambda = (b: Int, c: Int) => b + c
}
