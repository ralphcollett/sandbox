package sandbox

object MatchingSealed {

  sealed trait Colour
  case object Red extends Colour
  case object Orange extends Colour
  case object Blue extends Colour
  case object Pink extends Colour

  def performActionOnColour(colour: Colour) = {
    colour match {
      case Red => ""
      case Orange => ""
      case Blue => ""
      case Pink => ""
    }
  }
}
