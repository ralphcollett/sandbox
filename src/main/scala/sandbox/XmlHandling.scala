package sandbox

object XmlHandling extends App {

  val webPage =
    <html>
      <body>
        <h1>Header</h1>
      </body>
    </html>

  println((webPage \\ "h1").text)
}
