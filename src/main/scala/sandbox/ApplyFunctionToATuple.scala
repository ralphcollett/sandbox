package sandbox

object ApplyFunctionToATuple {

  def addition(a: Int, b: Int) = a + b

  val pairOfNumbers = (1, 2)
  addition(pairOfNumbers._1, pairOfNumbers._2) // Extract values and apply
  Function.tupled(addition _)(pairOfNumbers) // Scala 2.7 version
  (addition _).tupled(pairOfNumbers) // Scala 2.8 version
}
