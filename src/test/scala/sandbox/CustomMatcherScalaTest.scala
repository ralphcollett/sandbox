package sandbox

import java.time.LocalDate

import org.scalatest.{Matchers, FunSuite}
import org.scalatest.matchers.{MatchResult, Matcher}

class CustomMatcherScalaTest extends FunSuite with Matchers {

  test("Date Matcher") {
    def beBefore(otherDate: LocalDate) = new Matcher[LocalDate] {
      override def apply(date: LocalDate): MatchResult = {
        MatchResult(
          date.isBefore(otherDate),
          s"""Date $date is not before $otherDate""",
          s"""File $date is before $otherDate"""
        )
      }
    }
    LocalDate.of(2000, 10, 2) should beBefore(LocalDate.of(2000, 10, 3))
    LocalDate.of(2000, 10, 2) should not(beBefore(LocalDate.of(2000, 10, 2)))
    LocalDate.of(2000, 10, 2) should not(beBefore(LocalDate.of(2000, 10, 1)))
  }
}
